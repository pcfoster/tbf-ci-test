#!/bin/bash
set -e

echo "Variables: URLs, keys and hosts."
#############
# SSH key is saved in Repository Settings (https://bitbucket.org/pcfoster/tbf-ci-test/admin/addon/admin/pipelines/ssh-keys)
# and does not need to be added to pipeline container's .ssh dir in this script.
# GIT_REPO_URL is saved in Repository Variables (https://bitbucket.org/pcfoster/tbf-ci-test/admin/addon/admin/pipelines/repository-variables).
# Set for testing or "docroot"
DEPLOY_TO_PANTHEON_DIRECTORY="do-deploy"

# Remote repo host is not saved in Repository Settings > SSH keys > Known hosts form.  Don't track hosts:
echo "StrictHostKeyChecking no" >> "$HOME/.ssh/config"


echo "Testing access to Pantheon repo"
#############
git remote add pantheon "$GIT_REPO_URL"
git ls-remote --exit-code -h "$GIT_REPO_URL"


echo "Git push master branch to Pantheon"
#############
# Questions for those playing along at home:
# Do we squash commits?
# Do we do our subtree games here?  Will need to practice in local container?
# Or do we try adding a new .gitignore?
# Force flag?  does that kill the history?

# Subtree split sends pantheon only the changes within the DEPLOY_TO_PANTHEON_DIRECTORY (docroot).
# If no changes are found within docroot, git seems smart enough to fail gracefully; no new commit in the pantheon log.
# Do not use the force flag, that will overwrite the commit log in pantheon.
git push pantheon `git subtree split --prefix=$DEPLOY_TO_PANTHEON_DIRECTORY master`:master
